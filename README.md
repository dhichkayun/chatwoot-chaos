# Chaos testing for Chatwoot
This repo contains chaos tests for chatwoot deployment.

## Experiments covered
### 1. Pods
- pod delete
- container kill
- pod cpu hog
### 2. Network
- Network corruption(not working)
